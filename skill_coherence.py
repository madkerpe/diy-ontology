from abc import ABC
from typing import Dict, List, Optional

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from SkillEngine import ModelLoader
from SkillEngine.skill_profile import (ProbabilityProfileController, Profile,
                                       ProfileFactory)
from SkillEngine.taxonomy import Inventory, Taxonomy
from sklearn.decomposition import TruncatedSVD


class Coherence(ABC):
    """
    This is an abstract class to show the guaranteed signature of a coherence metric

    A coherence metric is a metric that can be used to measure similarity between skills and
    conditional probability of a skill given another skill, using the co-occurence of skills from various sources.
    """

    def similarity(self, skill_a: str, skill_b: str) -> float:
        """
        This denotes the similarity between two skills.
        Similarity is defined between -1 and 1

        :param skill_a: The first skill
        :param skill_b: The second skill
        :return: The similarity between the two skills, value between 0 and 1
        """
        raise NotImplementedError

    def conditional_probability(self, skill: str, given_skill: str) -> float:
        """
        This denotes the conditional probability of a skill given another skill
        meaning P(skill | given_skill), it is a value between 0 and 1

        :param skill: The skill for which the conditional probability is computed
        :param given_skill: The skill that is given
        :return: The conditional probability
        """
        raise NotImplementedError

    def mean_similarity(self, skill: str, given_skills: List[str]) -> float:
        """
        We define the similarity between a skill and a set of skills as the mean of the similarities between the skill and each skill in the set

        :param skill: The skill for which the similarity is computed
        :param given_skills: The skills that are given
        :return: The average of similarity(skill, given_skill[i]), value between -1 and 1
        """
        assert len(given_skills) == len(
            set(given_skills)), "given_skills can't contain duplicates"
        assert len(given_skills) > 0, "given_skills can't be empty"
        assert skill not in given_skills, "skill can't be in given_skills"

        return np.mean([
            self.similarity(skill, given_skill) for given_skill in given_skills
        ])

    def mean_conditional_probability(self, skill: str,
                                     given_skills: List[str]) -> float:
        """
        Since we can't find P(skill | given_skills) directly, we compute the mean of P(skill | given_skill) for all given_skills

        :param skill: The skill for which the conditional probability is computed
        :param given_skills: The skills that are given
        :return: The average of P(skill | given_skill[i]), value between 0 and 1
        """
        assert len(given_skills) == len(
            set(given_skills)), "given_skills can't contain duplicates"
        assert len(given_skills) > 0, "given_skills can't be empty"

        return np.mean([
            self.conditional_probability(skill, given_skill)
            for given_skill in given_skills
        ])

    def get_similarity_matrix(self, skills: List[str]) -> np.ndarray:
        """
        This is a very inefficient method, this should be overwritten by the child class!

        This denotes the similarity between a set of skills.
        It is a matrix of size (len(skills), len(skills)).
        This is intended to be symmetric.

        :param skills: A set of skills.
        :return: The similarity matrix between the skills, respecting the order of the skills list
        """
        matrix = np.zeros((len(skills), len(skills)))
        for i, skill_a in enumerate(skills):
            for j, skill_b in enumerate(skills):
                if i == j:
                    matrix[i, j] = 1.0
                if i > j:
                    value = self.similarity(skill_a, skill_b)
                    matrix[i, j] = value
                    matrix[j, i] = value

        return matrix

    def get_conditional_probability_matrix(self,
                                           skills: List[str]) -> np.ndarray:
        """
        This is a very inefficient method, this should be overwritten by the child class!

        This denotes the conditional probability between a set of skills.
        It is a matrix of size (len(skills), len(skills)).
        This is not intended to be symmetric.

        The row denotes the given skill, so P(A|B) = M[index(B), index(A)]

        :param skills: A set of skills.
        :return: The conditional probability matrix between the skills, respecting the order of the skills list
        """
        matrix = np.zeros((len(skills), len(skills)))
        for i, skill_a in enumerate(skills):
            for j, skill_b in enumerate(skills):
                if i == j:
                    matrix[i, j] = 1.0
                else:
                    matrix[i, j] = self.conditional_probability(
                        skill=skill_b, given_skill=skill_a)
        return matrix

    def plot_similarity_heatmap(self,
                                skills: List[str],
                                figsize: Optional[int] = 15):
        """
        Plots a heatmap of the similarity matrix between a set of skills

        :param skills: A set of skills
        :param figsize: The size of the (square) figure, defaults to 15
        """
        similarity_matrix = self.get_similarity_matrix(skills)

        fig, ax = plt.subplots(figsize=(figsize, figsize))
        _ = ax.imshow(similarity_matrix)

        # Show all ticks and label them with the respective list entries
        ax.set_xticks(np.arange(len(skills)), labels=skills)
        ax.set_yticks(np.arange(len(skills)), labels=skills)

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(),
                 rotation=45,
                 ha="right",
                 rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        for i in range(len(skills)):
            for j in range(len(skills)):
                value = f"{similarity_matrix[i, j]:.2f}"
                _ = ax.text(j, i, value, ha="center", va="center", color="w")

        ax.set_title("Similarity Heatmap")
        fig.tight_layout()
        plt.show()

    def plot_conditional_probability_heatmap(self,
                                             skills: List[str],
                                             figsize: Optional[int] = 15):
        """
        Plots a heatmap of the conditional probability matrix between a set of skills.
        Vertical axis is the given skill, horizontal axis is the skill that is being predicted.

        :param skills: A set of skills
        :param figsize: The size of the (square) figure, defaults to 15
        """
        conditional_probability_matrix = self.get_conditional_probability_matrix(
            skills)
        np.fill_diagonal(conditional_probability_matrix, 1.0)

        fig, ax = plt.subplots(figsize=(figsize, figsize))
        _ = ax.imshow(conditional_probability_matrix)

        # Show all ticks and label them with the respective list entries
        ax.set_xticks(np.arange(len(skills)), labels=skills)
        ax.set_yticks(np.arange(len(skills)), labels=skills)

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(),
                 rotation=45,
                 ha="right",
                 rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        for i in range(len(skills)):
            for j in range(len(skills)):
                value = f"{conditional_probability_matrix[i, j]:.4f}"
                _ = ax.text(j, i, value, ha="center", va="center", color="w")

        ax.set_title(
            "Conditional Probability Heatmap (given skill vertical, diagonal is set to 1.0)"
        )
        fig.tight_layout()
        plt.show()


class SkillClusterCoherenceHelper:
    """
    This class applies a coherence metric on a skill cluster.
    It is a wrapper around the Coherence class that implements some useful methods.

    :param coherence: A coherence metric.

    Example usage:
        from cw_utils.coherence import SentenceCoherence, SkillClusterCoherenceHelper

        SC = SentenceCoherence()
        SCH = SkillClusterCoherenceHelper(SC)

        SCH.coherence_of_skill_cluster(['Java', 'Python', 'Django', 'Software Development', 'Apache Spark', 'Mulching'])
    """

    def __init__(self, coherence: Coherence):
        self.coherence = coherence

    def coherence_of_skill_cluster(
            self,
            skill_cluster: List[str],
            use_conditional_probability: bool = False) -> float:
        """
        Computes the coherence of a skill cluster by taking the average 
        of the similarities between all skills in the skill cluster.
        This translates to the average of the upper triangle of the similarity matrix.

        If the conditional probability is used, we'll commpute the average of the conditional probabilities instead.
        This translates to the average of the conditional probability matrix without the diagonal.

        :param skill_cluster: A list of skills for which we're computing the coherence
        :param use_conditional_probability: Whether to use the conditional probability or the similarity, defaults to False
        :return: The coherence of the skill cluster
        """
        if len(skill_cluster) <= 1:
            Exception("Skill cluster is too small to compute coherence")

        if use_conditional_probability:
            matrix = self.coherence.get_conditional_probability_matrix(
                skill_cluster)
            matrix_without_diagonal_mask = ~np.eye(matrix.shape[0], dtype=bool)
            return matrix[matrix_without_diagonal_mask].mean()

        matrix = self.coherence.get_similarity_matrix(skill_cluster)
        upper_triangle_mask = np.triu_indices(matrix.shape[0], k=1)
        return matrix[upper_triangle_mask].mean()

    def coherence_of_taxonomy(self,
                              taxonomy: Taxonomy,
                              use_conditional_probability: bool = False):
        """
        Computes the coherence of a taxonomy by taking the average 
        of the coherence of all skill clusters in the taxonomy.

        :param taxonomy: The taxonomy for which we're computing the coherence
        :param use_conditional_probability: Whether to use the conditional probability or the similarity, defaults to False
        :return: The coherence of the taxonomy
        """
        return np.array([
            self.coherence_of_skill_cluster(
                list(skill_cluster.skills),
                use_conditional_probability=use_conditional_probability)
            for skill_cluster in taxonomy.competencies
        ])

    def coherence_between_skill_clusters(
            self,
            skill_cluster_a: List[str],
            skill_cluster_b: List[str],
            use_conditional_probability: bool = False):
        """
        Computes the coherence between two skill clusters by taking the average of the 
        similarities between all skills in skill cluster a and all skills in skill cluster b.
        This translates to the average of the similarity matrix between the two skill clusters.

        If the conditional probability is used, we'll compute the average of the conditional probabilities instead.

        :param skill_cluster_a: The first skill cluster
        :param skill_cluster_b: The second skill cluster
        :param use_conditional_probability: Whether to use the conditional probability or the similarity, defaults to False
        :return: The coherence between the two skill clusters
        """

        #TODO this is not the most efficient nor correct method possible
        #   we shouldn't compute the symmetric part and we don't account for OOV in InventoryCoherence similarity matrix

        skills = skill_cluster_a + skill_cluster_b
        if len(skills) <= 1:
            Exception("Skill clusters are too small to compute coherence")

        if use_conditional_probability:
            matrix = self.coherence.get_conditional_probability_matrix(skills)
            matrix1 = matrix[:len(skill_cluster_a), len(skill_cluster_a):]
            matrix2 = matrix[len(skill_cluster_a):, :len(skill_cluster_a)]
            return np.concatenate([matrix1, matrix2.T]).mean()

        matrix = self.coherence.get_similarity_matrix(skills)
        matrix = matrix[:len(skill_cluster_a), len(skill_cluster_a):]
        return matrix.mean()

    def most_likely_skill_cluster_in_taxonomy(
            self,
            skill: str,
            taxonomy: Taxonomy,
            top_n: Optional[int] = 1,
            use_conditional_probability: bool = False) -> Dict[str, float]:
        """
        Computes the most likely skill cluster for a skill in a taxonomy.
        It does this by computing the 'conditional probability' of the skill for every skill cluster (if implemented)
        if the conditional probability is not implemented, it will compute the mean similarity between the skill and every skill cluster.

        This returns a dictionary with the skill cluster name as key and the top_n (sorted) scores as value.

        :param skill: The skill for which the most likely skill cluster is computed
        :param taxonomy: The taxonomy in which the skill cluster is searched
        :param use_conditional_probability: Whether to use the conditional probability or the similarity, defaults to False
        :param top_n: The number of top scores to return, defaults to 1
        """
        scores_dict = {}
        for skill_cluster in taxonomy.competencies:
            skill_cluster_name = skill_cluster.name
            skill_cluster = list(skill_cluster.skills)

            if skill in skill_cluster:
                score = 1.0
            else:
                if use_conditional_probability:
                    score = self.coherence.mean_conditional_probability(
                        skill, skill_cluster)
                else:
                    score = self.coherence.mean_similarity(
                        skill, skill_cluster)

            scores_dict[skill_cluster_name] = score

        #return top_n elements
        return dict(
            sorted(scores_dict.items(), key=lambda item: item[1],
                   reverse=True)[:top_n])

    def most_likely_outlier_of_skill_cluster(
            self,
            skill_cluster: List[str],
            top_n: Optional[int] = 1,
            use_conditional_probability: bool = False) -> Dict[str, float]:
        """
        Returns the top_n most likely skill outliers for a given skill cluster.
        It does this by computing either the mean_similarity or the mean_conditional_probability 
        between the skill and every other skill in the skill cluster.

        :param skill_cluster: The skill cluster for which the most likely outlier is computed
        :param top_n: The number of top scores to return, defaults to 1
        :param use_conditional_probability: Whether to use the conditional probability or the similarity, defaults to False
        :return: A dictionary with the skill as key and the top_n (sorted) scores as value.
        """
        assert len(skill_cluster) == len(
            set(skill_cluster)), "skills can't contain duplicates"
        assert len(skill_cluster) > 0, "skills can't be empty"

        scores_dict = {}
        for skill in skill_cluster:
            if use_conditional_probability:
                score = self.coherence.mean_conditional_probability(
                    skill,
                    set(skill_cluster) - {skill})
            else:
                score = self.coherence.mean_similarity(
                    skill,
                    set(skill_cluster) - {skill})

            scores_dict[skill] = score

        #return the top_n elements
        return dict(
            sorted(scores_dict.items(),
                   key=lambda item: item[1],
                   reverse=False)[:top_n])

    def most_similar_skill_cluster_in_taxonomy(
            self,
            skill_cluster: List[str],
            taxonomy: Taxonomy,
            top_n: Optional[int] = 1,
            use_conditional_probability: bool = False) -> Dict[str, float]:
        """
        Returns the top_n most similar skill clusters in a taxonomy for a given skill cluster.
        It does this by using the coherence_between_skill_clusters method.

        :param skill_cluster: The skill cluster for which the most similar skill cluster is computed
        :param taxonomy: The taxonomy in which the skill cluster is searched
        :param top_n: The number of top scores to return, defaults to 1
        :param use_conditional_probability: Whether to use the conditional probability or the similarity, defaults to False
        :return: A dictionary with the skill cluster name as key and the top_n (sorted) scores as value.
        """

        scores_dict = {}
        for skill_cluster_from_taxonomy in taxonomy.competencies:
            skill_cluster_name = skill_cluster_from_taxonomy.name
            skill_cluster_from_taxonomy = list(
                skill_cluster_from_taxonomy.skills)

            score = self.coherence_between_skill_clusters(
                skill_cluster,
                skill_cluster_from_taxonomy,
                use_conditional_probability=use_conditional_probability)

            scores_dict[skill_cluster_name] = score

        #return top_n elements
        return dict(
            sorted(scores_dict.items(), key=lambda item: item[1],
                   reverse=True)[:top_n])


class SentenceCoherence(Coherence):
    """
    This Coherence class is based on the SVD decomposition of the PPMI matrix
    created from co-occurrence of skills in sentences
    It uses the sentence-ppmi-matrix that is present in the core

    Example usage:
        from cw_utils.coherence import SentenceCoherence

        SC = SentenceCoherence()
        SC.plot_similarity_heatmap(['Java', 'Python', 'Django', 'Software Development', 'Apache Spark', 'Mulching'])
        SC.similarity('Python', 'Django')
        SC.conditional_probability('Python', 'Django')
    """

    def __init__(self):
        self.sentence_ppmi = ModelLoader.get('sentence-ppmi-matrix', 'all')
        self.sentence_cond_prob = ModelLoader.get(
            'sentence-skill-conditional-probabilities', 'all')

    def similarity(self, skill_a, skill_b):
        matrix = self.sentence_ppmi.get_similarity_matrix([skill_a], [skill_b])
        return matrix[0, 0]

    def conditional_probability(self, skill: str, given_skill: str) -> float:
        if skill == given_skill:
            return 1.0
        return self.sentence_cond_prob.get_conditional_probability(
            skill, given_skill)

    def get_similarity_matrix(self, skills):
        matrix = self.sentence_ppmi.get_similarity_matrix(skills, skills)
        return matrix

    def get_conditional_probability_matrix(self,
                                           skills: List[str]) -> np.ndarray:
        skill_ids = np.array(self.sentence_ppmi._skill_to_id(skills))
        skill_ids = skill_ids[skill_ids != -1]
        matrix = self.sentence_cond_prob.get_conditional_probability_matrix(
            skill_indices=skill_ids,
            given_skill_indices=skill_ids,
            given_skills_probabilities=None)
        return matrix

    def get_embeddings(self, skills):
        return self.sentence_ppmi.ppmi_matrix[self.sentence_ppmi._skill_to_id(
            skills)]


class VacancyCoherence(Coherence):
    """
    This Coherence class is based on the SVD decomposition of the PPMI matrix
    created from co-occurrence of skills in vacancies from the datalake
    It uses the ppmi-matrix that is present in the core

    Example usage:
        from cw_utils.coherence import VacancyCoherence

        VC = VacancyCoherence()
        VC.plot_similarity_heatmap(['Java', 'Python', 'Django', 'Software Development', 'Apache Spark', 'Mulching'])
        VC.similarity('Python', 'Django')
        VC.conditional_probability('Python', 'Django')
    """

    def __init__(self):
        self.vacancy_ppmi = ModelLoader.get('ppmi-matrix', 'all')
        self.vacancy_cond_prob = ModelLoader.get(
            'skill-conditional-probabilities', 'all')

    def similarity(self, skill_a: str, skill_b: str) -> float:
        matrix = self.vacancy_ppmi.get_similarity_matrix([skill_a], [skill_b])
        return matrix[0, 0]

    def conditional_probability(self, skill: str, given_skill: str) -> float:
        if skill == given_skill:
            return 1.0
        return self.vacancy_cond_prob.get_conditional_probability(
            skill, given_skill)

    def get_similarity_matrix(self, skills: List[str]) -> np.ndarray:
        matrix = self.vacancy_ppmi.get_similarity_matrix(skills, skills)
        return matrix

    def get_conditional_probability_matrix(self,
                                           skills: List[str]) -> np.ndarray:
        skill_ids = np.array(self.vacancy_ppmi._skill_to_id(skills))
        skill_ids = skill_ids[skill_ids != -1]
        matrix = self.vacancy_cond_prob.get_conditional_probability_matrix(
            skill_indices=skill_ids,
            given_skill_indices=skill_ids,
            given_skills_probabilities=None)
        return matrix

    def get_embeddings(self, skills):
        return self.vacancy_ppmi.ppmi_matrix[self.vacancy_ppmi._skill_to_id(
            skills)]


class InventoryCoherence(Coherence):
    """
    This coherence class is based on the co-occurence of skills in a given inventory (list of skill profiles)
    Skills that occur frequently together in a profile are considered to be more similar
    This similarity is computed by an SVD decomposition (300 dimensions) of the PPMI matrix

    There is also functionality for conditional probabilities and its corresponding conditional probability matrix

    It assumes that the inventory won't be changed after initialization

    It is also used as a parent class for the other coherence classes that
    use the same coherence computation that starts from an inventory

    :param inventory: The inventory (SkillEngine class) of skills
    :param embeddings: The embeddings of the skills in the inventory, if None, they will be computed
    :param n_components: The number of components to use in the SVD decomposition, defaults to 300,
        can't be larger than the number of skills in the inventory

    Example usage:
        from SkillEngine.taxonomy import Inventory
        from cw_utils.coherence import skills_to_profile, InventoryCoherence

        custom_inv = Inventory([
            skills_to_profile(['Java', 'Django', 'Python']),
            skills_to_profile(['Java', 'Django', 'Python', 'Software Development']),
            skills_to_profile(['Python', 'Django']),
            skills_to_profile(['Java', 'Apache Spark']),
            skills_to_profile(['Java']),
            skills_to_profile(['Mulching'])
        ])

        IC = InventoryCoherence(custom_inv, n_components=3)
        IC.plot_similarity_heatmap(['Java', 'Python', 'Django', 'Software Development', 'Apache Spark', 'Mulching'])
        IC.conditional_probability('Python', 'Django')
        IC.similarity('Python', 'Django')
    """

    def __init__(self,
                 inventory: Inventory,
                 embeddings: Optional[np.ndarray] = None,
                 n_components=300):

        # inventory information
        self.inventory = inventory
        self.inventory_skills = list(inventory.skills.keys())
        self.skill2id = {
            skill: i
            for i, skill in enumerate(self.inventory_skills)
        }

        assert len(self.inventory_skills) > 0, "inventory can't be empty"

        # embeddings information
        if embeddings is None:
            assert len(
                self.inventory_skills
            ) > n_components, "n_components can't be larger than the number of skills in the inventory!"

            # compute neccessary information to compute embeddings
            self.cooccurrence_matrix = self._compute_cooccurrence_matrix(
                self.inventory_skills)
            self.conditional_probability_matrix = self._compute_conditional_probability_matrix(
                self.cooccurrence_matrix)
            self.skill_probabilities = self._compute_skill_probabilities(
                self.cooccurrence_matrix)
            self.shifted_ppmi_matrix = self._compute_shifted_ppmi_matrix(
                self.conditional_probability_matrix,
                self.skill_probabilities,
                shift_term=1)

            # actually compute the embeddings
            self.n_components = n_components
            self.embeddings = self._compute_embeddings(
                self.shifted_ppmi_matrix, self.n_components)

        else:
            assert embeddings.shape[0] == len(
                self.inventory_skills
            ), "embeddings must have the same number of rows as the number of skills in the inventory"
            self.n_components = embeddings.shape[1]
            self.embeddings = embeddings

            self.cooccurrence_matrix = None
            self.conditional_probability_matrix = None
            self.skill_probabilities = None
            self.shifted_ppmi_matrix = None

    def _compute_cooccurrence_matrix(self, skills: List[str]):
        """
        Computes a cooccurrence matrix, which is a matrix of size (len(skills), len(skills))
        It is symmetric and respects the order of the input skills list.
        Uses the skill_cooccurrence dictionary of the inventory attribute to compute the cooccurrence matrix.
        
        :param skills: A list of skills
        :return: The cooccurrence matrix between the skills, respecting the order of the input skills list
        """
        cooccurrence_matrix = np.zeros((len(skills), len(skills)))
        for i, skill_i in enumerate(skills):
            for j, skill_j in enumerate(skills):

                if i > j:
                    cooc_count = self.inventory.skill_cooccurrence.get(
                        frozenset((skill_i, skill_j)), 0.0)
                    cooccurrence_matrix[i, j] = cooc_count
                    cooccurrence_matrix[j, i] = cooc_count
                elif i == j:
                    cooccurrence_matrix[i, j] = self.inventory.skills.get(
                        skill_i, 0.0)
        return cooccurrence_matrix

    def _compute_conditional_probability_matrix(
            self, cooccurrence_matrix: np.ndarray) -> np.ndarray:
        """
        This computes a matrix of conditional probabilities between a set of skills
        based on the co-occurence matrix. Computed by the formula: P(skill, given_skill) / P(given_skill)
        which in an inventory translates to: Count(skill, given_skill) / Count(given_skill)
        It is a matrix of size (len(skills), len(skills))
        It is not (neccesarily) symmetric

        :param cooccurrence_matrix: a cooccurrence_matrix between skills
        :return: The conditional probability matrix between the skills, respecting the order of the cooccurrence matrix
        """
        skill_probabilities = cooccurrence_matrix.diagonal().reshape(-1, 1)
        conditional_probability_matrix = cooccurrence_matrix / skill_probabilities
        return conditional_probability_matrix

    def _compute_shifted_ppmi_matrix(
            self,
            conditional_probability_matrix: np.ndarray,
            skill_probabilities: np.ndarray,
            shift_term: Optional[int] = 1) -> np.ndarray:
        """
        Computes a matrix of shifted pointwise positive mutual information.

        The shifted pointwise positive mutual information between two skills
        It is a value between 0 and infinity and is symmetric in its input
        Shifting is a method that is emperically seen to improve results

        Computed by the formula: log(P(skill_a, skill_b) / (P(skill_a) * P(skill_b)))
        which in an inventory translates to: log2(P(skill_a | skill_b) / P(skill_a))
        which is then capped to above zero to get a positive value

        :param conditional_probability_matrix: The conditional probability matrix 
        between the skills, vertical axis is the given skill, horizontal axis is the skill that is being predicted
        :param skill_probabilities: The probabilities of occurence of each skill
        :return: The ppmi value between the skills
        """
        with np.errstate(divide="ignore"
                         ):  # ignore warning; we're assuming np.log(0) = -inf
            pmi_matrix = np.log2(conditional_probability_matrix /
                                 skill_probabilities)

        shifted_pmi_matrix = pmi_matrix - np.log2(shift_term)
        shifted_ppmi_matrix = np.maximum(shifted_pmi_matrix, 0.0)
        return shifted_ppmi_matrix

    def _compute_skill_probabilities(
            self, cooccurrence_matrix: np.ndarray) -> np.ndarray:
        """
        Computes the probabilities of occurence of each skill.
        Computed by taking the occurence of each skill and dividing it by the total number of occurences of all skills

        :param cooccurrence_matrix: a cooccurrence matrix between skills
        :returns: The probabilities of occurence of each skill
        """
        return cooccurrence_matrix.diagonal() / cooccurrence_matrix.sum()

    def _compute_embeddings(self, shifted_ppmi_matrix: np.ndarray,
                            n_components: int) -> np.ndarray:
        """
        Computes an embedding of the SVD decomposition of the shifted PPMI matrix.
        It is a matrix of size (n_skills, n_components)
        It respects the order of the input skills list and are normalized.

        :param shifted_ppmi_matrix: shifted ppmi matrix between skills
        :param n_components: The number of components to use in the SVD decomposition
        :return: An array with the embeddings of the skills with shape (n_skills, n_components)
        it respects the order of the input skills list
        """
        # compute embeddings
        transformer = TruncatedSVD(n_components=n_components, random_state=42)
        embeddings = transformer.fit_transform(
            shifted_ppmi_matrix)  # (n_skills, n_components)

        # normalize the embeddings
        norms = np.linalg.norm(embeddings, axis=1, keepdims=True)
        norms[norms <= 0.0] = 1.0  # handle possible zero vectors
        embeddings /= norms
        return embeddings

    def similarity(self, skill_a: str, skill_b: str) -> float:
        """
        The similarity is defined as a value between -1 and 1 and is computed by the dot product between the embeddings
        out of the SVD decomposition of the PPMI matrix of the inventory.
        
        If the skill is not in the inventory, it will return 0

        :param skill_a: The first skill
        :param skill_b: The second skill
        :return: The similarity between the two skills
        """
        try:
            skill_a_index = self.skill2id[skill_a]
            skill_b_index = self.skill2id[skill_b]
        except KeyError:
            return 0.0
        return np.dot(self.embeddings[skill_a_index],
                      self.embeddings[skill_b_index])

    def conditional_probability(self, skill: str, given_skill: str) -> float:
        """
        This denotes the conditional probability of a skill given another skill
        meaning P(skill | given_skill), it is a value between 0 and 1

        computed by the formula: P(skill, given_skill) / P(given_skill)
        which in an inventory translates to: Count(skill, given_skill) / Count(given_skill)
        Precomputed value is returned if available

        Skills that are not in the inventory will return 0.0

        :param skill: The skill for which the conditional probability is computed
        :param given_skill: The skill that is given
        :return: The conditional probability
        """

        if self.conditional_probability_matrix is None:
            return self.inventory.conditional_probability(skill, given_skill)

        try:
            skill_index = self.skill2id[skill]
            given_skill_index = self.skill2id[given_skill]

        except KeyError:
            return 0.0

        return self.conditional_probability_matrix[given_skill_index,
                                                   skill_index]

    def get_similarity_matrix(self, skills: List[str]) -> np.ndarray:
        """
        Get a subset of the similarity matrix between a set of skills.
        It is a matrix of size (len(skills), len(skills)).
        This is intended to be symmetric.

        :param skills: A set of skills that are present in the inventory.
        :return: The similarity matrix between the skills, respecting the order of the skills list
        """

        skill_ids = []
        for s in skills:
            if s in self.skill2id:
                skill_ids.append(self.skill2id[s])
            else:
                raise KeyError(f"Skill {s} not in inventory")

        skill_ids = np.array(skill_ids)
        embeddings_subset = self.embeddings[skill_ids]
        return np.dot(embeddings_subset, embeddings_subset.T)

    def get_conditional_probability_matrix(self,
                                           skills: List[str]) -> np.ndarray:
        """
        Get a subset of the conditional probability matrix between a set of skills.
        It is a matrix of size (len(skills), len(skills)).
        This is not intended to be symmetric.

        :param skills: A set of skills that are present in the inventory.
        :return: The conditional probability matrix between the skills, respecting the order of the skills list
        """

        assert self.conditional_probability_matrix is not None, "Conditional probability matrix is not computed when embeddings are given"

        skill_ids = []
        for s in skills:
            if s in self.skill2id:
                skill_ids.append(self.skill2id[s])
            else:
                raise KeyError(f"Skill {s} not in inventory")

        skill_ids = np.array(skill_ids)
        return self.conditional_probability_matrix[np.ix_(
            skill_ids, skill_ids)]

    def get_skill_probabilities(self, skills: List[str]) -> np.ndarray:
        """
        The probabilities of occurence of each skill.
        Computed by taking the occurence of each skill and dividing it by the total number of occurences of all skills

        :param skills: A set of skills that are present in the inventory.
        :return: The probabilities of occurence of each skill
        """

        assert self.skill_probabilities is not None, "Skill probabilities are not computed when embeddings are given"

        skill_ids = []
        for s in skills:
            if s in self.skill2id:
                skill_ids.append(self.skill2id[s])
            else:
                raise KeyError(f"Skill {s} not in inventory")
        skill_ids = np.array(skill_ids)
        return self.skill_probabilities[skill_ids]

    def get_cooccurrence_matrix(self, skills: List[str]) -> np.ndarray:
        """
        Get a subset of the cooccurrence matrix between a set of skills.
        It is a matrix of size (len(skills), len(skills)).
        This is intended to be symmetric.

        :param skills: A set of skills that are present in the inventory.
        :return: The cooccurrence matrix between the skills, respecting the order of the skills list
        """

        assert self.cooccurrence_matrix is not None, "Cooccurrence matrix is not computed when embeddings are given"

        skill_ids = []
        for s in skills:
            if s in self.skill2id:
                skill_ids.append(self.skill2id[s])
            else:
                raise KeyError(f"Skill {s} not in inventory")
        skill_ids = np.array(skill_ids)
        return self.cooccurrence_matrix[np.ix_(skill_ids, skill_ids)]

    def get_embeddings(self, skills: List[str]) -> np.ndarray:
        """
        Get the embeddings of a set of skills.
        It is a matrix of size (len(skills), n_components).

        :param skills: A set of skills that are present in the inventory.
        :return: The embeddings of the skills, respecting the order of the skills list
        """
        skill_ids = []
        for s in skills:
            if s in self.skill2id:
                skill_ids.append(self.skill2id[s])
            else:
                raise KeyError(f"Skill {s} not in inventory")
        skill_ids = np.array(skill_ids)
        return self.embeddings[skill_ids]

    def plot_cooccurrence_heatmap(self,
                                  skills: List[str],
                                  figsize: Optional[int] = 15):
        """
        Plots a heatmap of the conditional probability matrix between a set of skills.
        Vertical axis is the given skill, horizontal axis is the skill that is being predicted.

        :param skills: A set of skills
        :param figsize: The size of the (square) figure, defaults to 15
        """
        cooc_matrix = self.get_cooccurrence_matrix(skills)

        fig, ax = plt.subplots(figsize=(figsize, figsize))
        _ = ax.imshow(cooc_matrix)

        # Show all ticks and label them with the respective list entries
        ax.set_xticks(np.arange(len(skills)), labels=skills)
        ax.set_yticks(np.arange(len(skills)), labels=skills)

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(),
                 rotation=45,
                 ha="right",
                 rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        for i in range(len(skills)):
            for j in range(len(skills)):
                value = int(cooc_matrix[i, j])
                _ = ax.text(j, i, value, ha="center", va="center", color="w")

        ax.set_title("Cooccurrence Heatmap")
        fig.tight_layout()
        plt.show()


class DocumentCoherence(InventoryCoherence):
    """
    This coherence class is based on the co-occurence of skills in a set of documents (list of strings, e.g. vacancies)
    We perform MLST on these documents to get a list of skills per sentence per document.

    Each sentence is then seen as a skill profile and the InventoryCoherence class to compute the coherence.

    :param documents: A list of documents (strings)
    :param sentence_level: A boolean indicating whether to consider co-occurence of skills in sentences or in documents, 
        defaults to sentence_level
    """

    def __init__(self, documents=List[str], sentence_level: bool = True):
        self.language_id = ModelLoader.get('language-id', 'all')
        self.available_languages = ['en', 'nl', 'fr', 'de']
        extractions = self._mlst_proccess_documents(documents)
        skill_profiles = self._process_extractions(extractions, sentence_level)
        inventory = Inventory(skill_profiles)
        super().__init__(inventory=inventory)

    def _mlst_proccess_documents(self, document_list: List[str]) -> List[Dict]:
        """
        This function processes a list of documents (strings) and returns a list of skill profiles.
        It does this by performing MLST on the documents and then extracting the skills from the MLST output.

        :param document_list: A list of documents (strings)
        :return: A list of mlst extractions, one for each document
        """
        df = pd.DataFrame(document_list, columns=['document'])
        df['language'] = df['document'].map(self.language_id.predict)

        # filter out languages-not-available in mlst (so spacy) documents.
        df = df[df['language'].isin(self.available_languages)]

        # extract skills from documents using the languages that are present
        extractions = []
        for language in df['language'].unique():
            mlst = ModelLoader.get(
                'multilingual-skill-tagging-model-transformer',
                language=language)
            documents = df[df['language'] == language]['document'].tolist()
            extractions.extend(
                mlst.process_documents_bulk(
                    documents,
                    factory=ProfileFactory(ProbabilityProfileController())))

        return extractions

    def _process_extractions(self, extractions,
                             sentence_level: bool) -> List[Profile]:
        """
        This functions processes the extractions from the MLST model and returns a list of skill profiles.
        If sentence_level is True, it will return a list of skill profiles per sentence per document.
        If sentence_level is False, it will return a list of skill profiles per document.

        :param extractions: A list of extractions from the MLST model
        :param sentence_level: A boolean indicating whether to return a list of skill profiles per sentence or per document
        :return: A list of skill profiles
        """
        skill_lists = []
        for extraction in extractions:
            if sentence_level:
                for sentence_tuple in extraction['explanations']:
                    sentence_skills = sentence_tuple[1]
                    if len(sentence_skills) > 0:
                        skill_lists.append(sentence_skills)
            else:
                document_skills = list(extraction['skill_profile'])
                skill_lists.append(document_skills)

        skill_profiles = [skills_to_profile(p) for p in skill_lists]
        return skill_profiles


def skills_to_profile(skills: List[str]) -> Profile:
    """
    Helper function to create a basic probability skill_profile out of a list of skills,
    every skill is set to a probability of 1

    :param skills: A list of skills
    :return: A se-core skill_profile object (with an associated probability controller)
    """
    data = [{"name": skill, "probability": 1.} for skill in skills]
    return ProfileFactory(ProbabilityProfileController()).create(data=data)