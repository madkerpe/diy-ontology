from abc import ABC
from typing import Dict, List, Optional

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.decomposition import TruncatedSVD
from collections import Counter, defaultdict
import itertools


class Coherence(ABC):
    """
    This is an abstract class to show the guaranteed signature of a coherence metric

    A coherence metric is a metric that can be used to measure similarity between entities and
    conditional probability of occureance of an entitiy given another entity from various sources.
    """

    def similarity(self, entity_a: str, entity_b: str) -> float:
        """
        This denotes the similarity between two entities.
        Similarity is defined between -1 and 1

        :param entity_a: The first entity
        :param entity_b: The second entity
        :return: The similarity between the two entities, value between -1 and 1
        """
        raise NotImplementedError

    def conditional_probability(self, entity: str, given_entity: str) -> float:
        """
        This denotes the conditional probability of an entity given another entity based on co-occurence
        meaning P(entity | given_entity), it is a value between 0 and 1

        :param entity: The entity for which the conditional probability is computed
        :param given_entity: The entity that is given
        :return: The conditional probability based on co-occurence
        """
        raise NotImplementedError

    def get_similarity_matrix(self, entities: List[str]) -> np.ndarray:
        """
        This is a very inefficient method, this should be overwritten by the child class!

        This denotes the similarity between a set of entities.
        It is a matrix of size (len(entity), len(entity)).
        This is intended to be symmetric.

        :param entities: A set of entites.
        :return: The similarity matrix between the entities, respecting the order of the entities list
        """
        matrix = np.zeros((len(entities), len(entities)))
        for i, entity_a in enumerate(entities):
            for j, entity_b in enumerate(entities):
                if i == j:
                    matrix[i, j] = 1.0
                if i > j:
                    value = self.similarity(entity_a=entity_a, entity_b=entity_b)
                    matrix[i, j] = value
                    matrix[j, i] = value

        return matrix

    def get_conditional_probability_matrix(self, entities: List[str]) -> np.ndarray:
        """
        This is a very inefficient method, this should be overwritten by the child class!

        This denotes the conditional probability between a set of entities.
        It is a matrix of size (len(entities), len(entities)).
        This is not intended to be symmetric.

        The row denotes the given entity, so P(A|B) = M[index(B), index(A)]

        :param entities: A set of entities.
        :return: The conditional probability matrix between the entities, respecting the order of the entities list
        """
        matrix = np.zeros((len(entities), len(entities)))
        for i, entity_a in enumerate(entities):
            for j, entity_b in enumerate(entities):
                if i == j:
                    matrix[i, j] = 1.0
                else:
                    matrix[i, j] = self.conditional_probability(
                        entitiy=entity_b, given_entity=entity_a
                    )
        return matrix

    def plot_similarity_heatmap(self, skills: List[str], figsize: Optional[int] = 15):
        """
        Plots a heatmap of the similarity matrix between a set of skills

        :param skills: A set of skills
        :param figsize: The size of the (square) figure, defaults to 15
        """
        similarity_matrix = self.get_similarity_matrix(skills)
        np.fill_diagonal(similarity_matrix, 1.0)

        fig, ax = plt.subplots(figsize=(figsize, figsize))
        _ = ax.imshow(similarity_matrix)

        # Show all ticks and label them with the respective list entries
        ax.set_xticks(np.arange(len(skills)), labels=skills)
        ax.set_yticks(np.arange(len(skills)), labels=skills)

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        for i in range(len(skills)):
            for j in range(len(skills)):
                value = f"{similarity_matrix[i, j]:.2f}"
                _ = ax.text(j, i, value, ha="center", va="center", color="w")

        ax.set_title("Similarity Heatmap")
        fig.tight_layout()
        plt.show()

    def plot_conditional_probability_heatmap(
        self, skills: List[str], figsize: Optional[int] = 15, fill_diagonal: bool = True
    ):
        """
        Plots a heatmap of the conditional probability matrix between a set of skills.
        Vertical axis is the given skill, horizontal axis is the skill that is being predicted.

        :param skills: A set of skills
        :param figsize: The size of the (square) figure, defaults to 15
        """
        conditional_probability_matrix = self.get_conditional_probability_matrix(skills)
        if fill_diagonal:
            np.fill_diagonal(conditional_probability_matrix, 1.0)
        else:
            np.fill_diagonal(conditional_probability_matrix, 0.0)

        fig, ax = plt.subplots(figsize=(figsize, figsize))
        _ = ax.imshow(conditional_probability_matrix)

        # Show all ticks and label them with the respective list entries
        ax.set_xticks(np.arange(len(skills)), labels=skills)
        ax.set_yticks(np.arange(len(skills)), labels=skills)

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        for i in range(len(skills)):
            for j in range(len(skills)):
                value = f"{conditional_probability_matrix[i, j]:.4f}"
                _ = ax.text(j, i, value, ha="center", va="center", color="w")

        ax.set_title(
            "Conditional Probability Heatmap (given skill vertical, diagonal is set to 1.0)"
        )
        fig.tight_layout()
        plt.show()


class InventoryCoherence(Coherence):
    """
    This coherence class is based on the co-occurence of entities in a given inventory (list of list of entities in one document)
    Entities that occur frequently together in a document are considered to be more similar
    This similarity is computed by an SVD decomposition (default: 3 dimensions) of the PPMI matrix

    There is also functionality for conditional probabilities and its corresponding conditional probability matrix

    It assumes that the inventory won't be changed after initialization

    It is also used as a parent class for the other coherence classes that
    use the same coherence computation that starts from an inventory

    :param inventory: A list of list of entities in one document
    :param embeddings: The embeddings of the entities in the inventory, if None, they will be computed
    :param n_components: The number of components to use in the SVD decomposition, defaults to 300,
        can't be larger than the number of distinct entities in the inventory

    Example usage:

        custom_inv = [
            ['Java', 'Django', 'Python'],
            ['Java', 'Django', 'Python', 'Software Development'],
            ['Python', 'Django'],
            ['Java', 'Apache Spark'],
            ['Python'],
            ['Python'],
            ['Java'],
            ['Mulching']
        ]

        IC = InventoryCoherence(custom_inv, n_components=3)
        IC.plot_similarity_heatmap(['Java', 'Python', 'Django', 'Software Development', 'Apache Spark', 'Mulching'])
        IC.conditional_probability('Python', 'Django')
        IC.similarity('Python', 'Django')
    """

    def __init__(
        self,
        inventory: List[List[str]],
        embeddings: Optional[np.ndarray] = None,
        n_components: Optional[int] = 3,
        shift_term: Optional[int] = 1
    ):
        # inventory information
        self.inventory = inventory
        self.entity_count_dict = self.create_entity_count_dict(inventory)
        self.entity_cooccurrence_dict = self.create_entity_cooccurrence_dict(inventory)
        self.inventory_entities = list(self.entity_count_dict.keys())
        self.entity2id = {entity: i for i, entity in enumerate(self.inventory_entities)}

        assert len(self.inventory_entities) > 0, "inventory can't be empty"

        # embeddings information
        if embeddings is None:
            assert (
                len(self.inventory_entities) > n_components
            ), "n_components can't be larger than the number of unique entities in the inventory!"

            # compute neccessary information to compute embeddings
            self.cooccurrence_matrix = self._compute_cooccurrence_matrix(
                self.inventory_entities
            )
            self.conditional_probability_matrix = (
                self._compute_conditional_probability_matrix(self.cooccurrence_matrix)
            )
            self.entity_probabilities = self._compute_entity_probabilities(
                self.cooccurrence_matrix
            )
            self.shifted_ppmi_matrix = self._compute_shifted_ppmi_matrix(
                self.conditional_probability_matrix,
                self.entity_probabilities,
                shift_term=shift_term,
            )

            # actually compute the embeddings
            self.n_components = n_components
            self.embeddings = self._compute_embeddings(
                self.shifted_ppmi_matrix, self.n_components
            )

        else:
            assert embeddings.shape[0] == len(
                self.inventory_entities
            ), "embeddings must have the same number of rows as the number of unique entities in the inventory"
            self.n_components = embeddings.shape[1]
            self.embeddings = embeddings

            self.cooccurrence_matrix = None
            self.conditional_probability_matrix = None
            self.entity_probabilities = None
            self.shifted_ppmi_matrix = None

    @staticmethod
    def create_entity_count_dict(inventory) -> Dict[str, int]:
        """
        Return a dictionary of entities with their respective counts
        """
        counter = Counter()
        for document in inventory:
            counter.update(set(document))
        _entities = defaultdict(int)
        for entity, count in counter.items():
            _entities[entity] += count

        return dict(_entities)

    @staticmethod
    def create_entity_cooccurrence_dict(inventory):
        """
        Return a dictionary of entity pairs with their respective counts
        """
        counter = Counter()
        for occurences in inventory:
            pairs = [frozenset(pair) for pair in itertools.combinations(occurences, 2)]
            counter.update(pairs)
        entity_cooccurrence_dict = dict(counter)

        return entity_cooccurrence_dict

    def _compute_cooccurrence_matrix(self, entities: List[str]):
        """
        Computes a cooccurrence matrix, which is a matrix of size (len(entities), len(entities))
        It is symmetric and respects the order of the input entities list.
        Uses the entities_cooccurrence dictionary to compute the cooccurrence matrix.

        :param entities: A list of entities
        :return: The cooccurrence matrix between the entities, respecting the order of the input entities list
        """
        cooccurrence_matrix = np.zeros((len(entities), len(entities)))
        for i, entity_i in enumerate(entities):
            for j, entity_j in enumerate(entities):
                if i > j:
                    cooc_count = self.entity_cooccurrence_dict.get(
                        frozenset((entity_i, entity_j)), 0.0
                    )
                    cooccurrence_matrix[i, j] = cooc_count
                    cooccurrence_matrix[j, i] = cooc_count
                elif i == j:
                    cooccurrence_matrix[i, j] = self.entity_count_dict.get(entity_i, 0.0)
        return cooccurrence_matrix

    def _compute_conditional_probability_matrix(
        self, cooccurrence_matrix: np.ndarray
    ) -> np.ndarray:
        """
        This computes a matrix of conditional probabilities between a set of entities
        based on the co-occurence matrix. Computed by the formula: P(entity, given_entity) / P(given_entity)
        which in an inventory translates to: Count(entity, given_entity) / Count(given_entity)
        It is a matrix of size (len(entities), len(entities))
        It is not (neccesarily) symmetric

        :param cooccurrence_matrix: a cooccurrence_matrix between entities
        :return: The conditional probability matrix between the entities, respecting the order of the cooccurrence matrix
        """
        entity_probabilities = cooccurrence_matrix.diagonal().reshape(-1, 1)
        conditional_probability_matrix = cooccurrence_matrix / entity_probabilities
        return conditional_probability_matrix

    def _compute_shifted_ppmi_matrix(
        self,
        conditional_probability_matrix: np.ndarray,
        skill_probabilities: np.ndarray,
        shift_term: Optional[int] = 1,
    ) -> np.ndarray:
        """
        Computes a matrix of shifted pointwise positive mutual information.

        The shifted pointwise positive mutual information between two skills
        It is a value between 0 and infinity and is symmetric in its input
        Shifting is a method that is emperically seen to improve results

        Computed by the formula: log(P(skill_a, skill_b) / (P(skill_a) * P(skill_b)))
        which in an inventory translates to: log2(P(skill_a | skill_b) / P(skill_a))
        which is then capped to above zero to get a positive value

        :param conditional_probability_matrix: The conditional probability matrix
        between the skills, vertical axis is the given skill, horizontal axis is the skill that is being predicted
        :param skill_probabilities: The probabilities of occurence of each skill
        :return: The ppmi value between the skills
        """
        with np.errstate(
            divide="ignore"
        ):  # ignore warning; we're assuming np.log(0) = -inf
            pmi_matrix = np.log2(conditional_probability_matrix / skill_probabilities)

        shifted_pmi_matrix = pmi_matrix - np.log2(shift_term)
        shifted_ppmi_matrix = np.maximum(shifted_pmi_matrix, 0.0)
        return shifted_ppmi_matrix

    def _compute_entity_probabilities(
        self, cooccurrence_matrix: np.ndarray
    ) -> np.ndarray:
        """
        Computes the probabilities of occurence of each skill.
        Computed by taking the occurence of each skill and dividing it by the total number of occurences of all skills

        :param cooccurrence_matrix: a cooccurrence matrix between skills
        :returns: The probabilities of occurence of each skill
        """
        return cooccurrence_matrix.diagonal() / cooccurrence_matrix.sum()

    def _compute_embeddings(
        self, shifted_ppmi_matrix: np.ndarray, n_components: int
    ) -> np.ndarray:
        """
        Computes an embedding of the SVD decomposition of the shifted PPMI matrix.
        It is a matrix of size (n_skills, n_components)
        It respects the order of the input skills list and are normalized.

        :param shifted_ppmi_matrix: shifted ppmi matrix between skills
        :param n_components: The number of components to use in the SVD decomposition
        :return: An array with the embeddings of the skills with shape (n_skills, n_components)
        it respects the order of the input skills list
        """
        # compute embeddings
        transformer = TruncatedSVD(n_components=n_components, random_state=42)
        embeddings = transformer.fit_transform(
            shifted_ppmi_matrix
        )  # (n_skills, n_components)

        # normalize the embeddings
        norms = np.linalg.norm(embeddings, axis=1, keepdims=True)
        norms[norms <= 0.0] = 1.0  # handle possible zero vectors
        embeddings /= norms
        return embeddings

    def similarity(self, entity_a: str, entity_b: str) -> float:
        """
        The similarity is defined as a value between -1 and 1 and is computed by the dot product between the embeddings
        out of the SVD decomposition of the PPMI matrix of the inventory.

        If the skill is not in the inventory, it will return 0

        :param entity_a: The first skill
        :param entity_b: The second skill
        :return: The similarity between the two skills
        """
        try:
            entity_a_index = self.entity2id[entity_a]
            entity_b_index = self.entity2id[entity_b]
        except KeyError:
            return 0.0
        return np.dot(self.embeddings[entity_a_index], self.embeddings[entity_b_index])

    def conditional_probability(self, entity: str, given_entity: str) -> float:
        """
        This denotes the conditional probability of a skill given another skill
        meaning P(skill | given_skill), it is a value between 0 and 1

        computed by the formula: P(skill, given_skill) / P(given_skill)
        which in an inventory translates to: Count(skill, given_skill) / Count(given_skill)
        Precomputed value is returned if available

        Skills that are not in the inventory will return 0.0

        :param skill: The skill for which the conditional probability is computed
        :param given_skill: The skill that is given
        :return: The conditional probability
        """

        if self.conditional_probability_matrix is None:
            return self.inventory.conditional_probability(entity, given_entity)

        try:
            entity_index = self.entity2id[entity]
            given_entity_index = self.entity2id[given_entity]

        except KeyError:
            return 0.0

        return self.conditional_probability_matrix[given_entity_index, entity_index]

    def get_similarity_matrix(self, skills: List[str]) -> np.ndarray:
        """
        Get a subset of the similarity matrix between a set of skills.
        It is a matrix of size (len(skills), len(skills)).
        This is intended to be symmetric.

        :param skills: A set of skills that are present in the inventory.
        :return: The similarity matrix between the skills, respecting the order of the skills list
        """

        skill_ids = []
        for s in skills:
            if s in self.entity2id:
                skill_ids.append(self.entity2id[s])
            else:
                raise KeyError(f"Entity {s} not in inventory")

        skill_ids = np.array(skill_ids)
        embeddings_subset = self.embeddings[skill_ids]
        return np.dot(embeddings_subset, embeddings_subset.T)

    def get_conditional_probability_matrix(self, skills: List[str]) -> np.ndarray:
        """
        Get a subset of the conditional probability matrix between a set of skills.
        It is a matrix of size (len(skills), len(skills)).
        This is not intended to be symmetric.

        :param skills: A set of skills that are present in the inventory.
        :return: The conditional probability matrix between the skills, respecting the order of the skills list
        """

        assert (
            self.conditional_probability_matrix is not None
        ), "Conditional probability matrix is not computed when embeddings are given"

        skill_ids = []
        for s in skills:
            if s in self.entity2id:
                skill_ids.append(self.entity2id[s])
            else:
                raise KeyError(f"Entity {s} not in inventory")

        skill_ids = np.array(skill_ids)
        return self.conditional_probability_matrix[np.ix_(skill_ids, skill_ids)]

    def get_skill_probabilities(self, skills: List[str]) -> np.ndarray:
        """
        The probabilities of occurence of each skill.
        Computed by taking the occurence of each skill and dividing it by the total number of occurences of all skills

        :param skills: A set of skills that are present in the inventory.
        :return: The probabilities of occurence of each skill
        """

        assert (
            self.entity_probabilities is not None
        ), "Skill probabilities are not computed when embeddings are given"

        skill_ids = []
        for s in skills:
            if s in self.entity2id:
                skill_ids.append(self.entity2id[s])
            else:
                raise KeyError(f"Skill {s} not in inventory")
        skill_ids = np.array(skill_ids)
        return self.entity_probabilities[skill_ids]

    def get_cooccurrence_matrix(self, skills: List[str]) -> np.ndarray:
        """
        Get a subset of the cooccurrence matrix between a set of skills.
        It is a matrix of size (len(skills), len(skills)).
        This is intended to be symmetric.

        :param skills: A set of skills that are present in the inventory.
        :return: The cooccurrence matrix between the skills, respecting the order of the skills list
        """

        assert (
            self.cooccurrence_matrix is not None
        ), "Cooccurrence matrix is not computed when embeddings are given"

        skill_ids = []
        for s in skills:
            if s in self.entity2id:
                skill_ids.append(self.entity2id[s])
            else:
                raise KeyError(f"Skill {s} not in inventory")
        skill_ids = np.array(skill_ids)
        return self.cooccurrence_matrix[np.ix_(skill_ids, skill_ids)]

    def get_embeddings(self, skills: List[str]) -> np.ndarray:
        """
        Get the embeddings of a set of skills.
        It is a matrix of size (len(skills), n_components).

        :param skills: A set of skills that are present in the inventory.
        :return: The embeddings of the skills, respecting the order of the skills list
        """
        skill_ids = []
        for s in skills:
            if s in self.entity2id:
                skill_ids.append(self.entity2id[s])
            else:
                raise KeyError(f"Skill {s} not in inventory")
        skill_ids = np.array(skill_ids)
        return self.embeddings[skill_ids]

    def plot_cooccurrence_heatmap(self, skills: List[str], figsize: Optional[int] = 15):
        """
        Plots a heatmap of the conditional probability matrix between a set of skills.
        Vertical axis is the given skill, horizontal axis is the skill that is being predicted.

        :param skills: A set of skills
        :param figsize: The size of the (square) figure, defaults to 15
        """
        cooc_matrix = self.get_cooccurrence_matrix(skills)
        np.fill_diagonal(cooc_matrix, 0.0)

        fig, ax = plt.subplots(figsize=(figsize, figsize))
        _ = ax.imshow(cooc_matrix)

        # Show all ticks and label them with the respective list entries
        ax.set_xticks(np.arange(len(skills)), labels=skills)
        ax.set_yticks(np.arange(len(skills)), labels=skills)

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        for i in range(len(skills)):
            for j in range(len(skills)):
                value = int(cooc_matrix[i, j])
                _ = ax.text(j, i, value, ha="center", va="center", color="w")

        ax.set_title("Cooccurrence Heatmap")
        fig.tight_layout()
        plt.show()

